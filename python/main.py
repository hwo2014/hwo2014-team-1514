#-*-coding: utf-8 -*-

import json
import socket
import sys
from pprint import pprint

from consts import MAX_RECORD
from consts import SAVE_TRACK
from consts import PRINT_LOG
from util import get_cars
from util import get_track
from util import print_state
from util import save_track
from steering_controller import SteeringCon
from throttle_controller import ThrottleCon


class SJDriver(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.track_name = None
        self.car_count = 1
        self.passwd = 'adsf88z3dgwer325asf8v'
        self.key = key
        self.print_log = PRINT_LOG

        self.record = True

        self.track = []
        self.lanes = {}
        self.pieces = []
        self.angles = [0.0] * MAX_RECORD
        self.angle_diffs = [0.0] * MAX_RECORD
        self.prev_piece = 0
        self.prev_inpiece_distance = 0.0
        self.speed = [0.0] * MAX_RECORD
        self.track_model = []
        self.current_pos = (0, 0)
        self.current_lane = 0
        self.current_throttle = 0.8
        self.current_target_speed = 0.0
        self.turbo_available = False
        self.crash = False
        self.steering_msg_buffer = (None, None)
        self.angle_size = 0
        self.angle_dec = 0

        self.steering_con = SteeringCon(self)
        self.throttle_con = ThrottleCon(self)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        if not self.track_name:
            return self.msg("join", {"name": self.name, "key": self.key})
        else:
            return self.msg("joinRace", {
                "botId": {"name": self.name, "key": self.key},
                "trackName": self.track_name,
                "carCount": self.car_count}
            )

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        print("Game initiated")
        self.track = data['race']['track']
        if SAVE_TRACK:
            save_track(self.track)
        self.pieces = data['race']['track']['pieces']
        lanes = data['race']['track']['lanes']
        self.lanes = [0] * len(lanes)
        for lane in lanes:
            self.lanes[lane['index']] = lane['distanceFromCenter']
        self.throttle_con.set_turbo_zone()
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def set_my_car_data(self, my_car):
        """
        set my_car data to attributes
            self.angles: angle of my car
            self.angle_diffs : diff of car angles
            self.speed : speed of my_car
        """
        # set speed
        if self.prev_piece == my_car['piecePosition']['pieceIndex']:
            speed = my_car['piecePosition']['inPieceDistance'] - self.prev_inpiece_distance
        else:
            speed = self.speed[-1] + (self.speed[-1] - self.speed[-2])
        self.speed.append(speed)
        self.speed = self.speed[-MAX_RECORD:]
        self.prev_piece = my_car['piecePosition']['pieceIndex']
        self.prev_inpiece_distance = my_car['piecePosition']['inPieceDistance']

        # set angles & angle of diff
        self.angles.append(my_car['angle'])
        self.angles = self.angles[-MAX_RECORD:]
        self.angle_diffs.append(self.angles[-2] - self.angles[-1])
        self.angle_diffs = self.angle_diffs[-MAX_RECORD:]

        # set current_pos
        self.current_pos = (
            my_car['piecePosition']['pieceIndex'],
            my_car['piecePosition']['inPieceDistance']
        )

        # set current_lane
        self.current_lane = my_car['piecePosition']['lane']['startLaneIndex']

    def control(self, data, cars):
        track = get_track(self.pieces, self.current_pos)

        # change lane
        self.steering_con.steering(track, cars, self.lanes)
        if self.steering_msg_buffer[0] == 'left':
            self.msg('switchLane', 'Left')
            self.steering_msg_buffer = ('pending', self.steering_msg_buffer[1])
        elif self.steering_msg_buffer[0] == 'right':
            self.msg('switchLane', 'Right')
            self.steering_msg_buffer = ('pending', self.steering_msg_buffer[1])
        else:
            if self.steering_msg_buffer[1] != self.current_pos[0]:
                self.steering_msg_buffer = (None, None)
            
        # change throttle
        target_speed = self.throttle_con.take_max_target_speed(track)
        self.throttle_con.set_speed(target_speed)

    def on_car_positions(self, data):
        cars = get_cars(self.name, data)
        self.set_my_car_data(cars[0])
        self.control(data, cars)
        print_state(cars[0], self)

    def on_crash(self, data):
        if data['name'] == self.name:
            print("**** Crashed ****")
            pprint(data)
            print(self.angles[-5:])
            print(self.angle_diffs[-5:])
            print(self.speed[-5:])
            self.crash = True
            self.print_log = False
        self.ping()

    def on_spawn(self, data):
        if data['name'] == self.name:
            print ("**** Spawn ****")
            pprint(data)
            self.crash = False
            self.print_log = True
        self.ping()

    def on_lap_finished(self, data):
        print "Lab finished: ", data['lapTime']['millis']
        print 'Rank: ', data['ranking']['overall']
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        pprint(data)
        print
        try:
            my_car = filter(lambda x: x['car']['name'] == self.name, data['bestLaps'])[0]
            print 'BEST  :', my_car['result']['millis']
            my_car = filter(lambda x: x['car']['name'] == self.name, data['results'])[0]
            print 'RESULT:', my_car['result']['millis']
        except Exception:
            pprint(data)
        self.throttle_con.close()
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_turbo_available(self, data):
        self.turbo_available = True

    def on_turbo_start(self, data):
        self.turbo_available = False
        self.throttle_con.turbo_on = True

    def on_turbo_end(self, data):
        self.turbo_available = False
        self.throttle_con.turbo_on = False

    def turbo_on(self):
        if self.turbo_available:
            self.msg("turbo", "Pow pow pow pow pow")
            self.turbo_available = False
            self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'lapFinished': self.on_lap_finished,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'spawn': self.on_spawn,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) == 5:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = SJDriver(s, name, key)
        bot.run()
    elif len(sys.argv) == 6:
        host, port, name, key, track_name = sys.argv[1:6]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        print("Track={0}".format(track_name))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = SJDriver(s, name, key)
        bot.track_name = track_name
        bot.run()
    elif len(sys.argv) == 8:
        host, port, name, key, track_name, car_count, passwd = sys.argv[1:8]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        print("Track={0}".format(track_name))
        print("Car count={0}".format(car_count))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = SJDriver(s, name, key)
        bot.track_name = track_name
        bot.car_count = car_count
        bot.passwd = passwd
        bot.run()
    else:
        print("Usage: ./run host port botname botkey")
