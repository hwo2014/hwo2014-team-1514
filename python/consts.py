__author__ = 'Hyunsoo Park'

PRINT_LOG = True

SAVE_TRACK = False

MAX_SPEED = 15.0
MAX_RECORD = 100000
SENSE_DISTANCE = 3500
FINE_PIECE_LEN = 10.0
NUM_CURVATURE = 30
MAX_FINE_PIECES = 320
LEARNING_RATE = 1
MAX_RADIUS = 250
#TURBOABLE_DISTANCE = 450
TURBOABLE_DISTANCE = 300
TURBOABLE_RADIUS = (240, 220)
DISTANCE_METRIC = 'distance'
#DISTANCE_METRIC = 'radius'

INFINITY = 9999999
DEFAULT_TARGET_SPEED = 5.5

RANDOM_LANE_MODE = False
SHORTEST_PATH_MODE = False
COMPETITION_MODE = True