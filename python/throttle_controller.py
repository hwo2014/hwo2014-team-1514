#-*-coding: utf-8 -*-

__author__ = 'Hyunsoo Park'

from pprint import pprint
from util import round_length_of
from util import get_track
from consts import SENSE_DISTANCE
from consts import FINE_PIECE_LEN
from consts import MAX_FINE_PIECES
from consts import MAX_RADIUS
from consts import TURBOABLE_DISTANCE
from consts import TURBOABLE_RADIUS
from consts import MAX_SPEED
from consts import PRINT_LOG


class ThrottleCon(object):
    def __init__(self, driver):
        self.driver = driver
        self.turbo_on = False
        self.reward = []
        self.records = []
        self.targets = {}
        self.speed_limits = {}
        self.upper_bound_speed = {}
        self.lower_bound_speed = {}
        self.turbo_start_pos = None
        self.turbo_zone_length = TURBOABLE_DISTANCE / 10
        self.turbo_zone_avg_radius = TURBOABLE_RADIUS
        self.dummy = {}

        #### PID parameter ####
        self.int_t = 3
        #self.cs = [5.8, 3.30, 0.3, 0.0]
        self.cs = [5.8, 5.50, 0.3, 0.0]

        # apply
        self.diff = [0.0] * self.int_t
        self.c_p = self.cs[0]
        self.c_i = self.cs[1]
        self.c_d = self.cs[2]
        self.c_b = self.cs[3]
        self.c_min_throttle = 0.0

    def close(self):
        print 'DUMMY_DATA:'
        pprint(self.dummy)
        pass

    def set_speed(self, target_speed):
        current_speed = self.driver.speed[-1]
        diff = target_speed - current_speed

        self.diff.append(diff)
        self.diff = self.diff[-self.int_t:]
        integral = sum(self.diff)
        differ = self.diff[-2] - self.diff[-1]

        throttle = self.c_p * diff + \
            self.c_i * integral + \
            self.c_d * differ + \
            self.c_b

        # 출력 정규화
        throttle = max(throttle, -10)
        throttle = min(throttle, 10)
        throttle = throttle / 20.0 + 0.5
        throttle = max(throttle, 0.0)
        throttle = min(throttle, 1.0)

        # 위험한 수준의 각도에서 현재 이상의 속도로 가속하지 않음
        if abs(self.driver.angle_size) > 45 and self.driver.angle_dec != 1:
            prev_throttle = self.driver.current_throttle
            self.driver.current_throttle = min(throttle, prev_throttle)
        elif abs(self.driver.angle_size) > 40 and self.driver.angle_dec != 1:
            prev_throttle = self.driver.current_throttle
            self.driver.current_throttle = (throttle + 8.0 * prev_throttle) / 9.0
        elif abs(self.driver.angle_size) > 35 and self.driver.angle_dec != 1:
            prev_throttle = self.driver.current_throttle
            self.driver.current_throttle = (throttle + 7.0 * prev_throttle) / 8.0
        elif abs(self.driver.angle_size) > 30 and self.driver.angle_dec != 1:
            prev_throttle = self.driver.current_throttle
            self.driver.current_throttle = (throttle + 6.0 * prev_throttle) / 7.0
        elif abs(self.driver.angle_size) > 25 and self.driver.angle_dec != 1:
            prev_throttle = self.driver.current_throttle
            self.driver.current_throttle = (throttle + 5.0 * prev_throttle) / 6.0
        elif abs(self.driver.angle_size) > 20 and self.driver.angle_dec != 1:
            prev_throttle = self.driver.current_throttle
            self.driver.current_throttle = (throttle + 4.0 * prev_throttle) / 5.0
        elif abs(self.driver.angle_size) > 15 and self.driver.angle_dec != 1:
            prev_throttle = self.driver.current_throttle
            self.driver.current_throttle = (throttle + 3.0 * prev_throttle) / 4.0
        elif abs(self.driver.angle_size) > 5 and self.driver.angle_dec != 1:
            prev_throttle = self.driver.current_throttle
            self.driver.current_throttle = (throttle + 2.0 * prev_throttle) / 3.0
        else:
            self.driver.current_throttle = throttle
        self.driver.throttle(self.driver.current_throttle)

    def take_max_target_speed(self, track, *args, **kwargs):
        current_condition = self.get_features(track)

        # Turbo
        fine_pieces = current_condition[:MAX_FINE_PIECES]
        if self.driver.turbo_available:
            if self.need_turbo(fine_pieces, track):
                self.driver.turbo_on()

        turbo_on, angle, angle_dec = current_condition[-3:]
        current_speed = self.driver.speed[-1]
        current_radius_length = self.get_current_radius_length(fine_pieces)
        next_target_speed = self.get_next_radius_target_speed(fine_pieces)

        #### Distance to start breaking ####
        speed_diff = current_speed - next_target_speed
        if current_speed > 0.5 * next_target_speed:
            if current_speed > 15.0:
                msg = 'DEC 15'
                DEC_SPEED_ZONE_LEN = 72 * speed_diff
            elif current_speed > 14.0:
                msg = 'DEC 14'
                DEC_SPEED_ZONE_LEN = 70 * speed_diff
            elif current_speed > 13.0:
                msg = 'DEC 13'
                DEC_SPEED_ZONE_LEN = 68 * speed_diff
            elif current_speed > 12.0:
                msg = 'DEC 12'
                DEC_SPEED_ZONE_LEN = 66 * speed_diff
            elif current_speed > 11.0:
                msg = 'DEC 11'
                DEC_SPEED_ZONE_LEN = 64 * speed_diff
            elif current_speed > 10.0:
                msg = 'DEC 10'
                DEC_SPEED_ZONE_LEN = 62 * speed_diff
            elif current_speed > 9.0:
                msg = 'DEC 9'
                DEC_SPEED_ZONE_LEN = 60 * speed_diff
            elif current_speed > 8.0:
                msg = 'DEC 8'
                DEC_SPEED_ZONE_LEN = 58 * speed_diff
            elif current_speed > 7.5:
                msg = 'DEC 7.5'
                DEC_SPEED_ZONE_LEN = 56 * speed_diff
            elif current_speed > 7.0:
                msg = 'DEC 7'
                DEC_SPEED_ZONE_LEN = 54 * speed_diff
            elif current_speed > 6.5:
                msg = 'DEC 6.5'
                DEC_SPEED_ZONE_LEN = 52 * speed_diff
            elif current_speed > 6.0:
                msg = 'DEC 6.0'
                DEC_SPEED_ZONE_LEN = 50 * speed_diff
            elif current_speed > 5.5:
                msg = 'DEC 5.5'
                DEC_SPEED_ZONE_LEN = 48 * speed_diff
            elif current_speed > 5.0:
                msg = 'DEC 5.0'
                DEC_SPEED_ZONE_LEN = 46 * speed_diff
            elif current_speed > 4.5:
                msg = 'DEC 4.5'
                DEC_SPEED_ZONE_LEN = 42 * speed_diff
            else:
                msg = 'DEC else'
                DEC_SPEED_ZONE_LEN = 40 * speed_diff

            if self.driver.print_log:
                #print msg, DEC_SPEED_ZONE_LEN, '>', current_radius_length, DEC_SPEED_ZONE_LEN > current_radius_length
                #print current_speed - next_target_speed, current_speed, next_target_speed
                pass
        else:
            DEC_SPEED_ZONE_LEN = 200

        if DEC_SPEED_ZONE_LEN > current_radius_length:
            # 코너와 거리가 DEC_SPEED_ZONE_LEN 이하일때는 코너 진입 속도까지 점차 감속/가속
            diff = abs(current_speed) - abs(next_target_speed)
            d = float(current_radius_length) / DEC_SPEED_ZONE_LEN
            if abs(angle) > 50:
                next_target_speed *= 0.50
            elif abs(angle) > 40:
                next_target_speed *= 0.60
            elif abs(angle) > 30:
                next_target_speed *= 0.70
            elif abs(angle) > 20:
                next_target_speed *= 0.80
            elif abs(angle) > 10:
                next_target_speed *= 0.90
            else:
                next_target_speed *= 1.00
            target_speed = next_target_speed + d * diff
            #print 'A'
        else:
            dist = 20
            abs_fine_pieces = map(abs, fine_pieces)
            if sum(fine_pieces[:dist]) / float(dist) >= 230 and min(fine_pieces[:dist]) >= 225:
                # 직선
                #print 'S'
                target_speed = MAX_SPEED
            else:
                # 코너
                #print 'C'
                dist = 30
                abs_fine_pieces = map(abs, fine_pieces)
                min_radius = min(abs_fine_pieces[:dist])
                avg_radius = sum(abs_fine_pieces[:dist]) / float(dist)
                min_target_speed = self.get_target_speed(min_radius)
                avg_target_speed = self.get_target_speed(avg_radius)
                if 0 <= min_radius < 50:
                    #print 0
                    target_speed = 1.00 * min_target_speed + 0.00 * avg_target_speed
                elif 50 <= min_radius < 100:
                    #print 50
                    target_speed = 0.95 * min_target_speed + 0.05 * avg_target_speed
                elif 100 <= min_radius < 150:
                    #print 100
                    target_speed = 0.80 * min_target_speed + 0.20 * avg_target_speed
                elif 150 <= min_radius < 200:
                    #print 150
                    target_speed = 0.65 * min_target_speed + 0.35 * avg_target_speed
                else:
                    #print 200
                    target_speed = 0.50 * min_target_speed + 0.50 * avg_target_speed

                if angle_dec == 1:
                    # 코너를 탈출 중이라면 가속
                    #avg_radius = sum(abs_fine_pieces[:25]) / 25.0
                    min_radius = min(abs_fine_pieces[:dist])
                    #if self.driver.print_log:
                    #    print 'AVG:', min_radius
                    if min_radius > 245:
                        target_speed += 2.50
                    elif min_radius > 235:
                        target_speed += 2.25
                    elif min_radius > 225:
                        target_speed += 2.00
                    elif min_radius > 215:
                        target_speed += 1.75
                    elif min_radius > 205:
                        target_speed += 1.50
                    elif min_radius > 195:
                        target_speed += 1.25
                    elif min_radius > 185:
                        target_speed += 1.00
                    elif min_radius > 175:
                        target_speed += 0.75
                    elif min_radius > 165:
                        target_speed += 0.50
                    elif min_radius > 155:
                        target_speed += 0.25

                elif angle_dec == -1:
                    # 코너에 진입중이면 현재 각도에 따라 감속
                    if abs(angle) > 50:
                        target_speed *= 0.90
                    elif abs(angle) > 40:
                        target_speed *= 0.92
                    elif abs(angle) > 30:
                        target_speed *= 0.94
                    elif abs(angle) > 20:
                        target_speed *= 0.96
                    elif abs(angle) > 10:
                        target_speed *= 0.98

        self.driver.angle_size = abs(angle)
        self.driver.angle_dec = angle_dec
        self.driver.current_target_speed = target_speed
        return target_speed

    def get_fine_piece(self, track):
        fine_piece = []
        total_length = 0.0

        for idx, piece in enumerate(track):
            if piece['bend']:
                if piece['angle'] < 0:
                    distance_from_center = self.driver.lanes[self.driver.current_lane]
                else:
                    current_lane = -(self.driver.current_lane + 1)
                    distance_from_center = self.driver.lanes[current_lane]
                radius = abs(piece['radius'] + distance_from_center)
                current_piece_length = round_length_of(piece, distance_from_center)
            else:
                radius = MAX_RADIUS
                current_piece_length = piece['length']

            if idx == 0:
                current_piece_length -= self.driver.current_pos[1]

            total_length += current_piece_length
            for i in xrange(int(round(current_piece_length / FINE_PIECE_LEN))):
                fine_piece.append(radius)

            if total_length > SENSE_DISTANCE:
                break

        fine_piece = fine_piece[:MAX_FINE_PIECES]
        return fine_piece

    def get_features(self, track):
        fine_piece = self.get_fine_piece(track)
        if self.turbo_on:
            turbo_on = 1
        else:
            turbo_on = 0

        angle = self.driver.angles[-1]
        if self.driver.angles[-1] > 0. and self.driver.angle_diffs[-1] > 0 \
                or self.driver.angles[-1] < 0. and self.driver.angle_diffs[-1] < 0:
            angle_dec = 1
        elif self.driver.angles[-1] > 0. and self.driver.angle_diffs[-1] < 0 \
                or self.driver.angles[-1] < 0. and self.driver.angle_diffs[-1] > 0:
            angle_dec = -1
        else:
            angle_dec = 0
        return fine_piece + [turbo_on] + [angle] + [angle_dec]

    def get_current_radius_length(self, fine_piece):
        current_radius = fine_piece[0]
        length = 0.0
        for piece in fine_piece:
            if piece == current_radius:
                length += FINE_PIECE_LEN
            else:
                break
        return length

    def get_target_speed(self, radius):
        radius = abs(radius)
        #### 코너 진입속도 ####
        # 직선에 가까움
        #if self.driver.print_log:
            #print 'TH:',
        target_speed = 6.00
        if radius > 245:
            #if self.driver.print_log:
            #    print 250
            self.dummy.setdefault(250, 0)
            self.dummy[250] += 1
            target_speed = MAX_SPEED
        elif 245 >= radius > 235:
            #if self.driver.print_log:
            #    print 240
            self.dummy.setdefault(240, 0)
            self.dummy[240] += 1
            target_speed = 14.00        # 미국
        elif 235 >= radius > 225:
            #if self.driver.print_log:
            #    print 230
            self.dummy.setdefault(230, 0)
            self.dummy[230] += 1
            target_speed = 12.00        # 미국
        elif 225 >= radius > 215:
            #if self.driver.print_log:
            #    print 220
            self.dummy.setdefault(220, 0)
            self.dummy[220] += 1
            target_speed = 10.40        # 미국
        elif 215 >= radius > 205:  # 210
            #if self.driver.print_log:
            #    print 210
            self.dummy.setdefault(210, 0)
            self.dummy[210] += 1
            target_speed = 10.10        # 미국
        elif 205 >= radius > 195:  # 200
            #if self.driver.print_log:
            #    print 200
            self.dummy.setdefault(200, 0)
            self.dummy[200] += 1
            target_speed = 9.60        # 미국, 독일
        elif 195 >= radius > 185:  # 190
            #if self.driver.print_log:
            #    print 190
            self.dummy.setdefault(190, 0)
            self.dummy[190] += 1
            target_speed = 9.55         # 미국, 독일
        elif 185 >= radius > 175:
            #if self.driver.print_log:
            #    print 180
            self.dummy.setdefault(180, 0)
            self.dummy[180] += 1
            target_speed = 9.50         # 미국, 독일
        elif 175 >= radius > 165:
            #if self.driver.print_log:
            #    print 170
            self.dummy.setdefault(170, 0)
            self.dummy[170] += 1
            target_speed = 9.40
        elif 165 >= radius > 155:
            #if self.driver.print_log:
            #    print 160
            self.dummy.setdefault(160, 0)
            self.dummy[160] += 1
            target_speed = 8.70     # 독일
        elif 155 >= radius > 145:
            #if self.driver.print_log:
            #    print 150
            self.dummy.setdefault(150, 0)
            self.dummy[150] += 1
            target_speed = 8.25
        elif 145 >= radius > 135:
            #if self.driver.print_log:
            #    print 140
            self.dummy.setdefault(140, 0)
            self.dummy[140] += 1
            target_speed = 8.00     # 독일
        elif 135 >= radius > 125:
            #if self.driver.print_log:
            #    print 130
            self.dummy.setdefault(130, 0)
            self.dummy[130] += 1
            target_speed = 7.45
        elif 125 >= radius > 115:
            #if self.driver.print_log:
            #    print 120
            self.dummy.setdefault(120, 0)
            self.dummy[120] += 1
            target_speed = 7.40     # 독일
        elif 115 >= radius > 105:  # 110
            #if self.driver.print_log:
            #    print 110
            self.dummy.setdefault(110, 0)
            self.dummy[110] += 1
            target_speed = 7.125     # 핀란드, 독일
        elif 105 >= radius > 95:  # 100
            #if self.driver.print_log:
            #    print 100
            self.dummy.setdefault(100, 0)
            self.dummy[100] += 1
            target_speed = 6.975     # 핀란드
        elif 95 >= radius > 85:  # 90  $$ 고정
            #if self.driver.print_log:
            #    print 90
            self.dummy.setdefault(90, 0)
            self.dummy[90] += 1
            target_speed = 6.70     # 핀란드, 독일
        elif 85 >= radius > 75:
            #if self.driver.print_log:
            #    print 80
            self.dummy.setdefault(80, 0)
            self.dummy[80] += 1
            target_speed = 6.60     # 독일, 프랑스
        elif 75 >= radius > 65:
            #if self.driver.print_log:
            #    print 70
            self.dummy.setdefault(70, 0)
            self.dummy[70] += 1
            target_speed = 6.55     # 독일
        elif 65 >= radius > 55:
            #if self.driver.print_log:
            #    print 60
            self.dummy.setdefault(60, 0)
            self.dummy[60] += 1
            target_speed = 5.50     # 독일
        elif 55 >= radius > 45:
            #if self.driver.print_log:
            #    print 50
            self.dummy.setdefault(50, 0)
            self.dummy[50] += 1
            target_speed = 5.00
        elif 45 >= radius > 35:
            #if self.driver.print_log:
            #    print 40
            self.dummy.setdefault(40, 0)
            self.dummy[40] += 1     # 독일
            target_speed = 4.60
        elif 35 >= radius > 25:
            #if self.driver.print_log:
            #    print 30
            self.dummy.setdefault(30, 0)
            self.dummy[30] += 1
            target_speed = 4.30
        elif 25 >= radius:
            #if self.driver.print_log:
            #    print 20
            self.dummy.setdefault(20, 0)
            self.dummy[20] += 1
            target_speed = 4.00
        # 급한 코너
        return target_speed

    def get_next_radius_target_speed(self, fine_piece):
        current_radius = fine_piece[0]
        next_radius_idx = 0
        abs_fine_piece = map(abs, fine_piece)
        for idx, piece in enumerate(fine_piece):
            if piece is not current_radius:
                next_radius_idx = idx
                break

        avg_radius = sum(abs_fine_piece[next_radius_idx:next_radius_idx + 50]) / 50.0
        min_radius = min(abs_fine_piece[next_radius_idx:next_radius_idx + 50])
        radius = 0.2 * avg_radius + 0.8 * min_radius
        target_speed = self.get_target_speed(radius)
        return target_speed

    ## Turbo
    def need_turbo(self, *args):
        fine_pieces = args[0]
        abs_fine_pieces = map(abs, fine_pieces)
        length = self.turbo_zone_length
        avg_radius = float(sum(abs_fine_pieces[:length])) / float(length)
        min_radius = min(abs_fine_pieces[:length])
        if avg_radius > TURBOABLE_RADIUS[0] and min_radius > TURBOABLE_RADIUS[1]:
            return True
        else:
            return False

    def set_turbo_zone(self):
        tracks = get_track(self.driver.pieces, (0, 0))
        fine_pieces = self.get_fine_piece(tracks)

        zone_length = []
        # 길이 200 부터 1000까지 조사
        for start_pos in range(len(fine_pieces)):
            new_pieces = fine_pieces[start_pos:] + fine_pieces[:-(len(fine_pieces)-start_pos)]

            for length in range(10, 100):
                min_radius = min(new_pieces[:length])
                avg_radius = float(sum(new_pieces[:length])) / float(length)
                if avg_radius >= TURBOABLE_RADIUS[0] and min_radius >= TURBOABLE_RADIUS[1]:
                    zone_length.append((length, avg_radius))

        zone_length.sort(key=lambda x: x[0], reverse=True)
        if len(zone_length) > 0:
            length = zone_length[0][0]
            zone_length = filter(lambda x: x[0] == length, zone_length)
            zone_length.sort(key=lambda x: x[1], reverse=True)
            print zone_length

            self.turbo_zone_length = length
            self.turbo_zone_avg_radius = zone_length[0][1]
        else:
            self.turbo_zone_length = 50
            self.turbo_zone_avg_radius = TURBOABLE_RADIUS



