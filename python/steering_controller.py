#-*-coding: utf-8 -*-

__author__ = 'Hyunsoo Park'

import random
import operator
import consts
from util import get_track
from util import round_length_of


class SteeringCon(object):

    def __init__(self, driver):
        self.driver = driver

    def change_lane(self, target_lane):
        if self.driver.current_lane > target_lane:
            self.to_left()
        elif self.driver.current_lane < target_lane:
            self.to_right()

    def to_left(self):
        if self.driver.steering_msg_buffer[1] != self.driver.current_pos[0]:
            self.driver.steering_msg_buffer = ('left', self.driver.current_pos[0])

    def to_right(self):
        if self.driver.steering_msg_buffer[1] != self.driver.current_pos[0]:
            self.driver.steering_msg_buffer = ('right', self.driver.current_pos[0])

    def is_selectable_lane(self, current_position, lane):
        if abs(current_position - lane) <= 1:
            return True
        else:
            return False

    def find_shortest_path(self, track):
        next_track = track[1:]

        def _calc_dist(track, lane):
            length = 0
            for idx, piece in enumerate(track):
                if piece['switch']:
                    possible_lanes = filter(lambda x: max(lane, x) - min(lane, x) < 2, range(len(self.driver.lanes)))
                    sub_length = [(i, _calc_dist(track[idx+1:], i)) for i in possible_lanes]
                    sub_length.sort(key=lambda x: x[1])
                    length += sub_length[0][1]
                    break
                else:
                    if piece['bend']:
                        if piece['angle'] < 0:
                            distance_from_center = self.driver.lanes[lane]
                        else:
                            current_lane = -(lane + 1)
                            distance_from_center = self.driver.lanes[current_lane]
                        length += round_length_of(piece, distance_from_center)
                    else:
                        length += piece['length']
            return length

        distances = []
        my_car_lane = self.driver.current_lane
        possible_lanes = filter(lambda x: max(my_car_lane, x) - min(my_car_lane, x) < 2, range(len(self.driver.lanes)))
        for lane in possible_lanes:
            distances.append((lane, _calc_dist(next_track, lane)))
        return distances

    def find_lane_to_avoid_traffic_jam(self, track, lanes, cars):
        my_car = cars[0]  # 편의를 위해 내 차도 따로 변수에 저장
        nearest_car_distance_from_my_car = {}

        my_car_lane = self.driver.current_lane
        possible_lanes = filter(lambda x: max(my_car_lane, x) - min(my_car_lane, x) < 2, range(len(self.driver.lanes)))

        for lane in range(len(lanes)):
            if lane in possible_lanes:
                nearest_car_distance_from_my_car.setdefault(lane, consts.INFINITY)

        def get_piece_distance(piece):
            if piece['bend']:
                if piece['angle'] < 0:
                    distance_from_center = self.driver.lanes[lane]
                else:
                    current_lane = -(lane + 1)
                    distance_from_center = self.driver.lanes[current_lane]
                distance = round_length_of(piece, distance_from_center)
            else:
                distance = piece['length']
            return distance

        def distance_of(my_car, car):
            track = get_track(self.driver.pieces, (0, 0))
            distance = 0.0
            for idx, piece in enumerate(track):
                if idx > car['piecePosition']['pieceIndex']:
                    break
                distance += get_piece_distance(piece)
            distance += car['piecePosition']['inPieceDistance']

            for idx, piece in enumerate(track):
                if idx > my_car['piecePosition']['pieceIndex']:
                    break
                distance -= get_piece_distance(piece)
            distance -= my_car['piecePosition']['inPieceDistance']

            return distance

        for car_idx, car in enumerate(cars[1:]):
            # 각 레인별로 나와 가장 가까운 차의 거리를 구함
            # 현재 차 객체와 내 차와의 거리를 구하고
            distance_from_my_car = distance_of(my_car, car)
            lane = car['piecePosition']['lane']['endLaneIndex']
            if lane in possible_lanes:
                # 내 차와의 거리가 '양수' 라면 (즉 내차 앞에 있다면)
                if distance_from_my_car > 0:
                    # 만약 기존에 각 레인별로 저장된 가장 짧은 거리와 비교해 더 작은 값이라면, 현재 차와 나와의 거리로 바꾸어 저장
                    if nearest_car_distance_from_my_car[lane] > distance_from_my_car:
                        nearest_car_distance_from_my_car[lane] = distance_from_my_car

        return nearest_car_distance_from_my_car.items()

    def steering(self, track, cars, lanes):
        if len(self.driver.lanes) == 1:
            return
        else:
            if consts.RANDOM_LANE_MODE:
                if track[1]['switch']:
                    if random.random() > 0.5:
                        self.to_right()
                    else:
                        self.to_left()

            elif consts.SHORTEST_PATH_MODE:
                if track[1]['switch']:
                    distances = self.find_shortest_path(track[2:])
                    if consts.DISTANCE_METRIC == 'distance':
                        distances.sort(key=lambda x: x[1])
                    else:
                        distances.sort(key=lambda x: x[1], reverse=True)
                    if abs(distances[1][1] - distances[0][1]) > 1e-6:
                        lane_to_shortest_path = distances[0][0]

                        if lane_to_shortest_path > self.driver.current_lane:
                            self.to_right()
                        elif lane_to_shortest_path < self.driver.current_lane:
                            self.to_left()

            elif consts.COMPETITION_MODE:
                if track[1]['switch'] and abs(self.driver.angles[-1]) < 30:
                    # 최단경로 찾기
                    distances = self.find_shortest_path(track[2:])
                    distances.sort(key=lambda x: x[1])
                    if abs(distances[1][1] - distances[0][1]) > 1e-6:
                        lane_to_shortest_path = distances[0][0]
                    else:
                        lane_to_shortest_path = self.driver.current_lane

                    # 교통 체증 피하기
                    distances = self.find_lane_to_avoid_traffic_jam(track, lanes, cars)
                    distances.sort(key=lambda x: x[1], reverse=True)
                    empty_lane = None
                    if len(distances) > 1:
                        distance_my_lane = filter(lambda x: x[0] == self.driver.current_lane, distances)[0][1]
                        if distance_my_lane < 350:
                            if abs(distances[0][1] - distances[1][1]) > 50.0:
                                empty_lane = distances[0][0]

                    target_lane = lane_to_shortest_path
                    if empty_lane:
                        target_lane = empty_lane
                        print 'Avoid: to', target_lane
                        print empty_lane

                    if target_lane > self.driver.current_lane:
                        self.to_right()
                    elif target_lane < self.driver.current_lane:
                        self.to_left()