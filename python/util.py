__author__ = 'Hyunsoo Park'

import json
import math
from consts import MAX_RADIUS
from consts import PRINT_LOG


def get_cars(name, data):
    """
    get car data from data
    index 0: my car
    index x: others
    """
    my_car = None
    others = []
    for car_data in data:
        if car_data['id']['name'] == name:
            my_car = car_data
        else:
            others.append(car_data)
    return [my_car] + others


def get_track(pieces, current_pos):
    track = []
    _track = pieces[current_pos[0]:] + pieces[:current_pos[0]]
    for piece in _track:
        if 'switch' not in piece:
            piece['switch'] = False

        if 'angle' not in piece:
            piece['bend'] = False
        else:
            piece['bend'] = True

        track.append(piece)
    return track


def print_state(car, driver):
    current_target = driver.current_target_speed
    current_value = driver.speed[-1]

    track = get_track(driver.pieces, driver.current_pos)
    if track[0]['bend']:
        radius = track[0]['radius']
        piece_angle = track[0]['angle']
    else:
        radius = MAX_RADIUS
        piece_angle = 0

    if driver.print_log:
        print 'P: %03d %03d %03.3f %03.3f ,L: %d, A: %03.3f %03.3f, S: %03.3f, T: %03.3f - %03.3f = %03.3f, T: %1.2f, TB: %s' % (
            car['piecePosition']['pieceIndex'],
            radius, piece_angle,
            car['piecePosition']['inPieceDistance'],
            car['piecePosition']['lane']['endLaneIndex'],
            driver.angles[-1], driver.angle_diffs[-1],
            driver.speed[-1],
            current_target, current_value, current_target - current_value,
            driver.current_throttle,
            driver.throttle_con.turbo_on
        )


def save_track(track):
    path = './data/{0}.json'.format(track['id'])
    try:
        with open(path, 'wt') as f:
            json.dump(track, f, sort_keys=True, indent=4)
    except Exception:
        pass


def curvature_of(radius):
    return 360. / (2.0 * math.pi * radius)


def round_length_of(piece, distance_from_center=0.0):
    return 2. * math.pi * (piece['radius'] + distance_from_center) * abs(piece['angle']) / 360.
